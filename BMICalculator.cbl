       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMICALCULATOR.
       AUTHOR. NATCHA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  HEIGHT PIC 999V99 VALUE ZEROS.
       01  WEIGHT PIC 999V99 VALUE ZEROS.
       01  BMI PIC 99V99  VALUE ZEROS. 
       01  BMI-RESULT PIC x(25).
           88 UNDERWEIGHT VALUE "Underweight".
           88 NORMAL-WEIGHT VALUE "Normal weight".
           88 OVERWEIGHT VALUE "Overweight".
           88 OBESE VALUE "Obese".
           88 SEVERELY-OBESE VALUE "Severely Obese".
           88 MORBIDLY-OBESE VALUE "Morbidly Obese".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Input height (cm) - " WITH NO ADVANCING
           ACCEPT HEIGHT
           DISPLAY "Input weight (km) - " WITH NO ADVANCING
           ACCEPT WEIGHT 
           DIVIDE 100 INTO HEIGHT 
           COMPUTE BMI = WEIGHT / (HEIGHT * HEIGHT) 
           DISPLAY "BMI Score is "BMI 
           
           EVALUATE TRUE  
              WHEN  BMI < 18.5 SET UNDERWEIGHT TO TRUE
              WHEN  BMI < 25 SET NORMAL-WEIGHT TO TRUE
              WHEN  BMI < 30 SET OVERWEIGHT TO TRUE
              WHEN  BMI < 35 SET OBESE TO TRUE
              WHEN  BMI < 40 SET SEVERELY-OBESE TO TRUE
              WHEN  BMI >= 40 SET MORBIDLY-OBESE TO TRUE
           END-EVALUATE 
           DISPLAY BMI-RESULT 
           GOBACK 
           .  